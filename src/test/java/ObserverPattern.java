import com.changyu.zhy.observer.pattern.api.Observer;
import com.changyu.zhy.observer.pattern.api.Observerable;
import com.changyu.zhy.observer.pattern.impl.ObserverImpl;
import com.changyu.zhy.observer.pattern.impl.ObserverableImpl;
import com.changyu.zhy.observer.pattern.po.Weather;
import org.junit.Test;

/**
 * 描述：观察者模式测试
 *
 * @author zhanghaoyu
 * @date 2018/12/24 10:44
 **/
public class ObserverPattern {
    @Test
    public void observerTest(){
        ObserverableImpl observerable = new ObserverableImpl();
        Observer observer1 = new ObserverImpl();
        Observer observer2 = new ObserverImpl();
        observerable.registerObserver(observer1);
        observerable.registerObserver(observer2);

        Weather weather = new Weather();
        weather.setHumidity(23.23);
        weather.setPressure(12.12);
        weather.setTemperature(24.2);
        observerable.setWeather(weather);
    }
}
