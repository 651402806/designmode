import com.changyu.zhy.factory.pattern.one.Hamburger;
import com.changyu.zhy.factory.pattern.one.HamburgerFactory;
import org.junit.Test;

/**
 * 描述：工厂模式测试1
 *
 * @author zhanghaoyu
 * @date 2018/12/25 10:59
 **/
public class FactoryPatternOne {
    @Test
    public void test(){
        HamburgerFactory hamburgerFactory = new HamburgerFactory();
        Hamburger beefHamburger = hamburgerFactory.createHamburger("beefHamburger");
        Hamburger chickenHamburger = hamburgerFactory.createHamburger("chickenHamburger");
        beefHamburger.create();
        chickenHamburger.create();
    }
}
