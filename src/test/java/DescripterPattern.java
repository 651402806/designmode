import com.changyu.zhy.descripter.pattern.impl.*;
import org.junit.Test;

/**
 * 描述：装饰者模式测试
 *
 * @author zhanghaoyu
 * @date 2018/12/24 15:24
 **/
public class DescripterPattern {
    /**
     * 通过继承自同一个父类可以在子类中不断的增加父类所没有的功能
     * 又不需要每一种组合都新建一个类
     */
    @Test
    public void descripterTest(){
        BaseFood baseFood = new Hotpot();
        BaseSideDish beef = new Beef(baseFood);
        BaseSideDish potato = new Potato(beef);
        System.out.println(potato.getDescription());
        System.out.println(potato.costPrice());
    }
}
