package com.changyu.zhy.descripter.pattern.impl;

/**
 * 描述：小菜(因小菜可能存在于baseFood不同的方法所以新建了一个抽象类)
 *
 * @author zhanghaoyu
 * @date 2018/12/24 15:09
 **/
public abstract class BaseSideDish extends BaseFood{
    @Override
    public abstract String getDescription();
}
