package com.changyu.zhy.descripter.pattern.impl;

/**
 * 描述：食品基础类
 *
 * @author zhanghaoyu
 * @date 2018/12/24 14:54
 **/
public abstract class BaseFood {
    public String description;

    public String getDescription() {
        return description;
    }
    public abstract double costPrice();
}
