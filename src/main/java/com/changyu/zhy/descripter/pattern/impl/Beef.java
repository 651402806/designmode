package com.changyu.zhy.descripter.pattern.impl;

/**
 * 描述：牛肉
 *
 * @author zhanghaoyu
 * @date 2018/12/24 15:00
 **/
public class Beef extends BaseSideDish{
    private BaseFood baseFood;
    public Beef(BaseFood baseFood) {
        this.baseFood=baseFood;
    }

    @Override
    public String getDescription() {
        return baseFood.getDescription()+" beef";
    }

    @Override
    public double costPrice() {
        return baseFood.costPrice()+20;
    }
}
