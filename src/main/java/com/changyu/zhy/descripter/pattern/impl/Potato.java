package com.changyu.zhy.descripter.pattern.impl;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2018/12/24 15:21
 **/
public class Potato extends BaseSideDish{
    private BaseFood baseFood;

    public Potato(BaseFood baseFood) {
        this.baseFood = baseFood;
    }

    @Override
    public String getDescription() {
        return baseFood.getDescription()+" potato";
    }

    @Override
    public double costPrice() {
        return baseFood.costPrice()+8;
    }
}
