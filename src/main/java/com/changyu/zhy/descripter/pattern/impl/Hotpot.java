package com.changyu.zhy.descripter.pattern.impl;

/**
 * 描述：火锅类
 *
 * @author zhanghaoyu
 * @date 2018/12/24 14:57
 **/
public class Hotpot extends BaseFood{
    public Hotpot(){
        description="hotpot";
    }
    @Override
    public double costPrice() {
        //火锅汤底50
        return 50;
    }
}
