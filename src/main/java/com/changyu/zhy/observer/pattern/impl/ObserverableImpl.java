package com.changyu.zhy.observer.pattern.impl;

import com.changyu.zhy.observer.pattern.api.Observer;
import com.changyu.zhy.observer.pattern.api.Observerable;
import com.changyu.zhy.observer.pattern.po.Weather;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2018/12/24 10:15
 **/
public class ObserverableImpl implements Observerable {
    /**
     * 被观察者维护的观察者列表
     */
    private List<Observer> observerList;

    /**
     * 被观察者维护的信息
     */
    private Weather weather;

    public ObserverableImpl() {
        observerList=new ArrayList<Observer>();
    }

    public void registerObserver(Observer observer) {
        observerList.add(observer);
    }

    public void removeObserver(Observer observer) {
        observerList.remove(observer);
    }

    public void notifyObserver() {
        for (Observer observer : observerList) {
            observer.update(weather);
        }
    }

    public void setWeather(Weather weather){
        this.weather=weather;
        notifyObserver();
    }
}
