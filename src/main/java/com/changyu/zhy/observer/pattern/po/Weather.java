package com.changyu.zhy.observer.pattern.po;

import lombok.Data;

/**
 * 描述：天气实体
 *
 * @author zhanghaoyu
 * @date 2018/12/24 10:25
 **/
@Data
public class Weather {
    /**
     * 温度
     */
    private double temperature;
    /**
     * 湿度
     */
    private double humidity;
    /**
     * 压力
     */
    private double pressure;
}
