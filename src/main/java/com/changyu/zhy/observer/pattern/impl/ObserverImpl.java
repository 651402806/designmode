package com.changyu.zhy.observer.pattern.impl;

import com.changyu.zhy.observer.pattern.api.Observer;
import com.changyu.zhy.observer.pattern.po.Weather;

/**
 * 描述：观察者实现
 *
 * @author zhanghaoyu
 * @date 2018/12/24 10:34
 **/
public class ObserverImpl implements Observer {
    public void update(Object object) {
        if(object instanceof Weather){
            Weather weather = (Weather) object;
            System.out.println(weather.getHumidity()+" "+weather.getPressure()+" "+weather.getTemperature());
        }
    }
}
