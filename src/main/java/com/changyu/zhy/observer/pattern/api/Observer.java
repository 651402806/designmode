package com.changyu.zhy.observer.pattern.api;

/**
 * 描述：观察者接口
 *
 * @author zhanghaoyu
 * @date 2018/12/24 10:14
 **/
public interface Observer {
    /**
     * 当被观察者更新数据的时候此方法会被回调
     * @param object
     */
    void update(Object object);
}
