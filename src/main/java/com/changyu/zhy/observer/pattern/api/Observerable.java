package com.changyu.zhy.observer.pattern.api;

/**
 * 描述：被观察者接口
 *
 * @author zhanghaoyu
 * @date 2018/12/24 10:09
 **/
public interface Observerable {
    /**
     * 每当增加一个观察者的时候调用此方法进行注册
     * @param observer
     */
    void registerObserver(Observer observer);

    /**
     * 每当观察者需要取消订阅的时候调用此方法
     * @param observer
     */
    void removeObserver(Observer observer);

    /**
     * 每当被观察者数据变化的时候调用此方法通知观察者
     */
    void notifyObserver();
}
