package com.changyu.zhy.factory.pattern.one;

/**
 * 描述：牛肉汉堡
 *
 * @author zhanghaoyu
 * @date 2018/12/25 10:54
 **/
public class BeefHamburger extends Hamburger{
    @Override
    public void description() {
        System.out.println("BeefHamburger");
    }
}
