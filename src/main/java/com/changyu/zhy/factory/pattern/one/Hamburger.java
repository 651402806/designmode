package com.changyu.zhy.factory.pattern.one;

/**
 * 描述：汉堡接口
 *
 * @author zhanghaoyu
 * @date 2018/12/25 10:39
 **/
public abstract class Hamburger {
    /**
     * 切碎面包
     */
    public void cut(){
        System.out.println("切碎面包");
    }

    /**
     * 放进蔬菜
     */
    public void putVegetables(){
        System.out.println("放蔬菜进去");
    }

    /**
     * 制作
     */
    public void create(){
        cut();
        putVegetables();
        description();
    }

    /**
     * 描述
     */
    public abstract void description();
}
