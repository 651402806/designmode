package com.changyu.zhy.factory.pattern.one;

/**
 * 描述：鸡肉汉堡
 *
 * @author zhanghaoyu
 * @date 2018/12/25 10:46
 **/
public class ChickenHamburger extends Hamburger{
    @Override
    public void description() {
        System.out.println("ChickenHamburger");
    }
}
