package com.changyu.zhy.factory.pattern.one;

/**
 * 描述：汉堡工厂(简单工厂模式)
 * 缺点:如果制作子类特有的汉堡，需要改变工厂，但是特有的不方便写在公共的从长里面
 * 如果重新写两个新的工厂的话，又会出现类爆炸的趋势，所以一般考虑使用工厂方法模式将对象的
 * 实现封装在子类的一个方法中，而父类仅仅提供一个抽象的方法给子类使用
 *
 * @author zhanghaoyu
 * @date 2018/12/25 10:55
 **/
public class HamburgerFactory {
    private String beefHamburger="beefHamburger";
    private String chickenHamburger="chickenHamburger";

    /**
     * 工厂模式将创建对象的行为放到一个具体的类或方法中
     * 通过指定名称来创建指定的对象，这样客户端改代码的时候只需要改动type就可以了
     * @param type 汉堡类型
     * @return 汉堡实例
     */
    public Hamburger createHamburger(String type){
        if(beefHamburger.equals(type)){
            return new BeefHamburger();
        }else if(chickenHamburger.equals(type)){
            return new ChickenHamburger();
        }
        return null;
    }
}
